import { MEMBER_PORTAL } from '../../../config/App';

export default {
  items: [
    {
      name: 'HOME',
      url: '/',
    },
    {
      name: 'PERUSAHAAN',
      url: '/company',
    },
    {
      name: 'PRODUK',
      children: [
        {
          name: 'Jenis Produk',
          url: '/product/profile',
        },
        {
          name: 'Harga Produk',
          url: '/product/price',
        },
      ],
    },
  ],
};
