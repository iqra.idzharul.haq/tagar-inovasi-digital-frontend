import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
// import product from '../../../assets/img/product.png';
import product from '../../../assets/img/product.jpg';

class GeneralCard extends Component <any,any> {
  render() {
    const { image, title, description, reversed } = this.props;
    return (
      <div className="animated fadeIn">
        {
          !reversed &&
          <Card className="min-height-300 p-4">
            <CardBody>
              <Row>
                <Col md="6" lg="6" className="text-align-center">
                  <img src={image}/>
                </Col>
                <Col md="6" lg="6">
                  <h1>{title}</h1>
                  <p>{description}</p>
                  <Button className="btn-default">Learn More</Button>
                </Col>
              </Row>
            </CardBody>
          </Card>
        }
        {
          reversed &&
          <div className="min-height-300 p-4">
            <CardBody>
              <Row>
                <Col md="6" lg="6" className="text-align-right">
                  <h1>{title}</h1>
                  <p>{description}</p>
                  <Button className="btn-default">More Info</Button>
                </Col>
                <Col md="6" lg="6" className="text-align-center">
                  <img src={image}/>
                </Col>
              </Row>
            </CardBody>
          </div>
        }
      </div>
    )
  }
}

export default GeneralCard;