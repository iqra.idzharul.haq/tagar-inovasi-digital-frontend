import React, { Component } from "react";
import { Row, Col, Button, Input } from "reactstrap";
import nojs from '../../../assets/img/nojs.png';

class ContactForm extends Component <any,any> {
  render() {
    const { title } = this.props;
    return (
      <div className="animated fadeIn pad-100">
        <Col>
          <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row className="mar-bot-50">
            <Col className=" text-align-center">
              {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p> */}
            </Col>
          </Row>
          <Row>
            <Col lg="6" md="6">
                <p>Name</p>
                <Input placeholder="masukkan nama" type="text"/>
                <p>Email</p>
                <Input placeholder="masukkan email" type="text"/>
                <p>Messages</p>
                <Input type="textarea" placeholder="masukkan pesan" rows={5}></Input>
                <br/>
                <Button className="btn-default">Submit</Button>
            </Col>
            <Col lg="6" md="6">
              <div className="product-exellence text-align-center pad-50-100" style={{height: '100%'}}>
                <h4 className="bold">Alamat</h4>
                <p>Gedung The Convergence Indonesia Lt. 30, Kawasan Rasuna Epicentrum, Jl. HR. Rasuna Said, Kel. Karet Kuningan, Kec. Setiabudi, Jakarta Selatan 12940</p>
                <hr></hr>
                <h4 className="bold">Nomor Telepon</h4>
                <p>02122535575</p>
                <hr></hr>
                <h4 className="bold">Email</h4>
                <p>tagarinovasidigital@gmail.com</p>
              </div>
            </Col>
          </Row>
          {/* <Row>
            {
              data && data.map((item)=>{
                return(
                  <Col className=" text-align-center">
                    <Card className="min-height-300 p-4">
                      <CardHeader>
                        <h3>{item.name}</h3>
                      </CardHeader>
                      <CardBody>
                      <div className="member-icon mar-bot-30">
                        <img src={item.url}/>
                      </div>
                      <p>{item.description}</p>
                      </CardBody>
                    </Card>
                  </Col>
                )
              })
            }
          </Row> */}
        </Col>
      </div>
    )
  }
}

export default ContactForm;