import React, { useState } from 'react';
import sadewa from '../../../assets/img/sadewa.png';
import yudistira from '../../../assets/img/yudistira.png';
import bima from '../../../assets/img/bima.png';
import arjuna from '../../../assets/img/arjuna.png';
import nakula from '../../../assets/img/nakula.png';
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption, Row, Col } from 'reactstrap';

const items = [
  {
    id: 1,
    altText: 'Yudhistira',
    title: 'YUDHISTIRA',
    caption: 'Beras pandan wangi murni yang berasal dari cianjur tanpa bahan pengawet dan tanpa pewangi buatan',
    price: "70.000",
    image: yudistira,
  },
  {
    id: 2,
    altText: 'Bima',
    title: 'BIMA',
    caption: 'Beras poles Premium yang berasal dari solo dan sragen, beras ini sama dengan beras konvensional yaitu: setra ramos,kurmo',
    price: "67.500",
    image: bima,
  },
  {
    id: 3,
    altText: 'Arjuna',
    title: 'ARJUNA',
    caption: 'Beras pandan wangi premium yang berasal dari ciparay,tanpa bahan pengawet dan pewangi buatan',
    price: "62.500",
    image: arjuna,
  },
  {
    id: 4,
    altText: 'Nakula',
    title: 'NAKULA',
    caption: 'Beras Jembar Medium yang berasal dari sumedang',
    price: "59.500",
    image: nakula,
  },
  {
    id: 5,
    altText: 'Sadewa',
    title: 'SADEWA',
    caption: 'Beras medium kelas 1 yang berasal dari Banjar',
    price: "56.500",
    image: sadewa,
  }
];

const Example = () => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex: React.SetStateAction<number>) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        className="carousel-body"
        tag="div"
        key={item.id}
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
      >
        <div className="mar-bot-30">
          <Row >
            <Col>
            <div className="carousel-image">
              <img src={item.image} alt="product"/>
            </div>
            </Col>
            <Col>
            <div className="carousel-image">
              <h1 className="mar-bot-30 mar-top-40">{item.title}</h1>
              <h5 className="mar-bot-30">{item.caption}</h5>
              <h5 className="bold">Rp.{item.price}/pack (5kg)</h5>
            </div>
            {/* <CarouselCaption className="carousel-description" captionText={item.caption} captionHeader={item.title} /> */}
            </Col>
          </Row>
        </div>
      </CarouselItem>
    );
  });

  return (
    <div className="pad-100-0">
      <Col>
        <Row className="mar-bot-15">
          <Col className=" text-align-center">
            <h1>PRODUK KAMI</h1>
          </Col>
        </Row>
        <Row>
          <Col className=" text-align-center">
            <p>Tagar Inovasi Digital adalah produk yang unggul dalam aroma dan cita rasa, 
              <br></br>cocok untuk dijual secara langsung kepada konsumen ataupun untuk konsumsi pribadi</p>
          </Col>
        </Row>
      </Col>
      <Carousel
        activeIndex={activeIndex}
        next={next}
        previous={previous}
      >
        <CarouselIndicators  items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
        {slides}
        {/* <CarouselControl  direction="prev" directionText="Previous" onClickHandler={previous} />
        <CarouselControl  direction="next" directionText="Next" onClickHandler={next} /> */}
      </Carousel>
    </div>
  );
}

export default Example;