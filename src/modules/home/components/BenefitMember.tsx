import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody } from "reactstrap";
// import product from '../../../assets/img/product.png';
import product from '../../../assets/img/product.jpg';
import ec from '../../../assets/img/ec.jpg';
import es from '../../../assets/img/es.png';
import pay from '../../../assets/img/pay.png';
import GeneralCard from "./GeneralCard";

class BenefitMember extends Component <any,any> {
  state={
    data:[
      {
        title: 'E-Commerce',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        url: ec,
        reversed: false,
      },
      {
        title: 'Digital Transaction',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        url: pay,
        reversed: true,
      },
      {
        title: 'Enterprise System',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        url: es,
        reversed: false,
      },
    ]
  }
  render() {
    const { title } = this.props;
    const { data } = this.state;
    return (
      <div className="animated fadeIn pad-100" style={{paddingBottom: '0px'}}>
        <Col>
          {/* <Row className="mar-bot-15">
            <Col className=" text-align-center">
              <h1>{title}</h1>
            </Col>
          </Row>
          <Row className="mar-bot-50">
            <Col className=" text-align-center">
              <p>Bisnis yang dilakukan oleh Tagar Inovasi Digital</p>
            </Col>
          </Row> */}
          <Row>
            {
              data && data.map((item)=>{
                return(
                  <GeneralCard image={item.url} title={item.title} description={item.description} reversed={item.reversed}></GeneralCard>
                )
              })
            }
          </Row>
        </Col>
      </div>
    )
  }
}

export default BenefitMember;