import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { getProductExcellence } from "../../../utilities/api";

class ProductExcellence extends Component <any,any> {
  render() {
    return(
      <div className="product-exellence pad-200-15">
        <Row>
            <Col className=" text-align-center" md="4" lg="4">
              <h1>27</h1>
              <h5>Happy Clients</h5>
            </Col>
            <Col className=" text-align-center" md="4" lg="4">
              <h1>43</h1>
              <h5>Awesome Projects</h5>
            </Col>
            <Col className=" text-align-center" md="4" lg="4">
              <h1>2</h1>
              <h5>Year Experiences</h5>
            </Col>
        </Row>
      </div>
    )
  }
}

export default ProductExcellence;