import React, { Component, Fragment } from 'react';
import nojs from '../../../assets/img/nojs.png';
import product from '../../../assets/img/product.jpg';
import ProductComponent from '../components/ProductComponent';
import ProductExcellence from '../components/ProductExcellence';
import PageTitle from '../../company/components/PageTitle';
import BenefitMember from '../components/BenefitMember';
import ContactForm from '../components/ContactForm';
import { Button } from 'reactstrap';
import GeneralCard from '../components/GeneralCard';

class Home extends Component<any, any> {
  render() {
    return (
      <div>
        <div className="top-wrapper">
          <div className="container">
            <h1>TAGAR INOVASI DIGITAL</h1>
            <h4>Tagar Inovasi Digital Adalah <br></br>Perusahaan Jasa Keuangan Berbasis Teknologi.</h4>
            <Button onClick={()=>this.props.history.push('/perusahaan')}>More Info</Button>
          </div>
        </div>
        <ProductExcellence title="KEUNGGULAN PRODUK"/>
        <BenefitMember title="WE OFFER"/>
        <ContactForm title="HUBUNGI KAMI"></ContactForm>
      </div>
    )
  }
}

export default Home;
