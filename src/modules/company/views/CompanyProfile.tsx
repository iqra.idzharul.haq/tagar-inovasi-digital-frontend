import React, { Component } from "react";
import PageTitle from "../components/PageTitle";
import BenefitCompany from "../components/BenefitCompany";
import AboutCompany from "../components/AboutCompany";

class CompanyProfile extends Component<any, any> {
  render(){
    return (
      <div className="animated fadeIn">
      <PageTitle />
      {/* <AboutCompany /> */}
      <BenefitCompany />
      </div>
    )
  }
}

export default CompanyProfile;