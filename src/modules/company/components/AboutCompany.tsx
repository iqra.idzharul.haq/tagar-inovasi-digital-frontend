import React, { Component } from "react";
import logo from '../../../assets/img/brand/logo.png';
import { Row, Col } from "reactstrap";

class AboutCompany extends Component<any, any> {
  render(){
    return (
      <div>
        <Row className="mar-bot-15 p-4">
          <Col lg="4" md="6">
            <div><img className="about-logo" src={logo} /></div>
          </Col>
          <Col lg="8" md="6">
            <p>Tagar Inovasi Digital adalah Perusahaan Direct Selling/Network Marketing yang berbasis pada produk berkualitas dan konsumsi sehari hari dengan support system yang dapat diikuti oleh seluruh mitranya</p>
          </Col>
        </Row>
      </div>
    )
  }
}

export default AboutCompany;