import React, { Component } from "react";

class PageTitle extends Component<any, any> {
  render(){
    return (
      <div className="benefit-company">
        <div className="vision">
          <h3>Visi <span>TAGAR INOVASI DIGITAL</span></h3>
          <p>Menjadi Perusahaan yang bermanfaat bagi masyarakat melalui sistem pemasaran jaringan yang murah, mudah dan kuat.</p>
        </div>
        <div className="mission">
        <h3>Misi <span>TAGAR INOVASI DIGITAL</span></h3>
        <ol>
          <li>Membuka peluang usaha sebesar-besarnya dan sebanyak-banyaknya melalui kemitran tagar inovasi digital dengan sistem Penjualan Langsung dan Pemasaran Berjenjang.</li>
          <li>Mengelola usaha inti dan pelayanan kemitraan dengan amanah dan professional.</li>
          <li>Membina mitra tagar inovasi digital dalam pemahaman atas Produk dan Strategi Pemasaran melalui pelatihan dan pemahaman.</li>
        </ol>
        </div>
      </div>
    )
  }
}

export default PageTitle;